import io
import base64
import numpy as np
from PIL import Image
import matplotlib.image as mpimg



def open_image_as_array(path):
    return mpimg.imread(path)

def to_image(arr):
    return Image.fromarray(arr)

def get_encoded_img(arr):
    img = to_image(arr).convert("L")
    img_byte_arr = io.BytesIO()
    img.save(img_byte_arr, format='PNG')
    my_encoded_img = base64.encodebytes(img_byte_arr.getvalue()).decode('ascii')
    return my_encoded_img

def apply_mask(img, mask):
    mask *= (mask > 0.5)
    new_img = img.copy()
    new_img[np.where(mask)] = 1
    return new_img * 255
