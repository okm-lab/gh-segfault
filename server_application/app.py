# from distutils.log import debug
import os
import io
from flask import Flask, request, jsonify, send_file
from flask_cors import CORS
from segmentator.fault_segmentator import FaultSegmentator
import json
from image_tools import itools

app = Flask(__name__)
CORS().init_app(app=app, send_wildcard=True)

segmentator = FaultSegmentator("segmentator/model.pt")

# @app.route("/predict", methods=["GET"])
# def predict():
#     imagefile = flask.request.files.get('imagefile', '')


if not os.path.exists("images"):
    os.mkdir("images")

@app.route("/predict", methods=["POST"])
def predict():

    if 'file' not in request.files:
        print("no file in")
        return 'there is no file1 in form!'
    print("file in")
    file = request.files['file']
    path = os.path.join("images", file.filename)
    file.save(path)
    image = itools.open_image_as_array(path)
    print(itools.open_image_as_array(path).shape)
    prediction = segmentator.predict_slice(image)
    return jsonify({
                "code": 0,
                "image": itools.get_encoded_img(itools.apply_mask(image, prediction))
            })
app.run()