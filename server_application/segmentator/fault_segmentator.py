import torch
import numpy as np
import albumentations as A
import segmentation_models_pytorch as smp
from albumentations.pytorch.transforms import ToTensorV2

class FaultSegmentator:
    def __init__(self, path=None):
        self.model = smp.Unet(
            encoder_name="efficientnet-b3",
            encoder_weights="imagenet",
            in_channels=3,
            classes=1
        )
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        if path:
            self.load(path)

    def load(self, path):
        checkpoint = torch.load(path, map_location=self.device)
        self.model.load_state_dict(checkpoint['model_state_dict'])
        
    def predict_slice(self, image_slice, pad_size=16, predict_size=128, write_mask_size=96, repeat_input=3):
        self.model.eval()

        mask_slice = np.zeros(image_slice.shape)
        pad_mask_x = mask_slice.shape[0] % write_mask_size
        pad_mask_y = mask_slice.shape[1] % write_mask_size
        mask_slice = np.pad(mask_slice, [(0, pad_mask_x), (0, pad_mask_y)], mode='constant')
        image_slice = np.pad(image_slice, [(pad_size, pad_size), (pad_size, pad_size)], mode='edge')

        add_pad = (image_slice.shape[0] - pad_size * 2) % write_mask_size
        if add_pad != 0:
            image_slice = np.pad(image_slice, [(0, add_pad), (0, 0)], mode='edge')
        add_pad = (image_slice.shape[1] - pad_size * 2) % write_mask_size
        if add_pad != 0:
            image_slice = np.pad(image_slice, [(0, 0), (0, add_pad)], mode='edge')
        x_steps = (image_slice.shape[0] - pad_size * 2) // write_mask_size
        y_steps = (image_slice.shape[1] - pad_size * 2) // write_mask_size


        for i in range(x_steps):
            for j in range(y_steps):
                input_image = image_slice[i * write_mask_size: i * write_mask_size + predict_size,
                                          j * write_mask_size: j * write_mask_size + predict_size]
                input_image = self.scale(input_image)
                input_image = np.repeat(input_image[..., None], repeat_input, axis=2)
                input_image = self.get_image_transformer()(image=input_image)['image']
                with torch.no_grad():
                    output_mask = self.model(input_image[None, ...].to(self.device)).sigmoid().squeeze().cpu().numpy()
                output_mask = output_mask[pad_size:-pad_size, pad_size:-pad_size]
                mask_slice[i * write_mask_size:(i + 1) * write_mask_size,
                           j * write_mask_size:(j + 1) * write_mask_size] = output_mask
        return mask_slice[: mask_slice.shape[0] - pad_mask_x, : mask_slice.shape[1] - pad_mask_y]
    
    @staticmethod
    def get_image_transformer():
        return A.Compose([
            A.Normalize(max_pixel_value=1.0),
            ToTensorV2()
        ])
    
    @staticmethod
    def scale(image):
        return (image - image.min()) / (image.max() - image.min())
# fsegmentator = FaultSegmentator(path="model.pt")