import './App.css';
import React, {useState, useEffect} from "react";
import ReactDOM from 'react-dom';


const sendImage = () => {
    const handleSubmit = (e) => {
        e.preventDefault()
        const formData = new FormData(e.target);
        const Upload = async() => {
            await fetch('http://127.0.0.1:5000/predict', {
                method: 'POST',
                body: formData
            })
                .then(resp => resp.json())
                .then(resp => {
                    const data = resp.image
                    console.log(data)
                    const Image = ({ data }) => <img src={`data:image/png;base64,${data}`}  alt=""/>
                    ReactDOM.render(<Image data={data} />, document.getElementById('container'))
                })
        }
        Upload();
    }

    return (
        <form onSubmit={handleSubmit} className="container mt-5 pt-5 pb-5" encType="multipart/form-data">
            <div className="form-inline justify-content-center mt-5">
                <label htmlFor="image" className="ml-sm-4 font-weight-bold mr-md-4">Image : </label>
                <div className="input-group">
                    <input type="file" id="image" name="file"
                           accept="image/*" className="file-custom"/>
                </div>
                {/*<img src="" id="preview" />*/}
            </div>
            <div className="input-group justify-content-center mt-4">
                <button type="submit" className="btn btn-md btn-primary">Upload...</button>
            </div>
        </form>
    )
}


const FileUploader = props => {

    const hiddenFileInput = React.useRef(null);

    const handleClick = event => {
        hiddenFileInput.current.click()
    }

    const handleChange = event => {
        const fileUploaded = event.target.files[0];
        props.handleFile(fileUploaded)
    }

    return (
        <>
            <button>Upload image</button>
            <input type={"file"} style={{display: 'none'}} ref={hiddenFileInput} onChange={handleChange}/>
        </>
    )
}


function UploadImages() {
    const [images, setImages] = useState([])
    const [imageURLs, setImageURLs] = useState([])

    useEffect(() => {
        if (images.length < 1) return;
        const newImageUrls = [];
        images.forEach(image => newImageUrls.push(URL.createObjectURL(image)));
        setImageURLs(newImageUrls);
    }, [images])

    function onImageChange(e) {
        setImages([...e.target.files])
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        const formData = new FormData(e.target);
        const Upload = async() => {
            await fetch('http://127.0.0.1:5000/predict', {
                method: 'POST',
                body: formData
            })
                .then(resp => resp.json())
                .then(resp => {
                    const data = resp.image
                    console.log(data)
                    const Image = ({ data }) => <img src={`data:image/png;base64,${data}`}  alt=""/>
                    ReactDOM.render(<Image data={data} />, document.getElementById('container'))
                })
        }
        Upload();
    }


    return (
        <form onSubmit={handleSubmit} className="container mt-3 pt-3 pb-3" encType="multipart/form-data">
            <div className="form-inline mt-2">
                <label htmlFor="image" className="ml-sm-1 font-weight-bold mr-md-4">Image : </label>
                <div className="input-group">
                    <input type="file" id="image" name="file" onChange={onImageChange}
                           accept="image/*" className="file-custom"/>
                </div>
            </div>
            <div className="input-group mt-3">
                <button type="submit" className="btn btn-md btn-primary">Upload...</button>
            </div>

            <div id="img-wrapper">
                <div>{imageURLs.map(imageSrc => <img src={imageSrc} key={imageSrc} className="preview mt-3 ml-5"/>)}</div>
                <div id="container" className="preview mt-3 ml-5"/>
            </div>
        </form>

    )
}


function App() {
  return (
      <div className="App">
          <nav className="navbar navbar-light bg-light">
              <span className="navbar-brand mb-0 h1">Segmentation Fault</span>
          </nav>

        <header className="App-header">

           {UploadImages()}
         </header>
       </div>
  );
}

export default App;
